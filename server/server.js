const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
  user: "root",
  host: "localhost",
  password: "rootpassword",
  database: "sakilabdd",
});



// Create API endpoint
app.get("/movies", (req, res) => {
//   console.log(req.body);
//   const page_length = req.body.page_length;
  db.query(
    `
        SELECT f.title, f.rental_rate, f.rating, c.name, N.nb_of_rent
        FROM sakila.film as f, sakila.film_category as fc, sakila.category as c,
            (
                SELECT COUNT(p.rental_id) as nb_of_rent, i.film_id as film_id
                FROM sakila.rental as r, sakila.inventory as i, sakila.payment as p
                WHERE 
                    i.inventory_id = r.inventory_id and
                    r.rental_id = p.rental_id
                GROUP BY i.film_id
            ) as N
        WHERE 
            f.film_id = fc.film_id and
            fc.category_id = c.category_id and
            N.film_id = f.film_id
        ORDER BY f.title
        LIMIT 10;`,
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
        // console.log(req.body);
      }
    }
  );
});

app.listen(3001, () => {
  console.log("Yaaaay ! Server running on port 3001");
});
